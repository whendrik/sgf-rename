# SGF Rename

An SGF rename tool in python.

Say current directory contains `.sgf` files, which you want to rename, in format `"{PB} - {PW}, {DT} - {RE}"`,

Where `PB`, `PW`, `DT`, and `RE` are SGF properties, see

https://en.wikipedia.org/wiki/Smart_Game_Format

than

```
./sgf_rename.py --format "{PB} - {PW}, {DT} - {RE}" *.sgf
```

will rename all `SGF` files.

For the first run, install the python requirements

```
pip install -r requirements.txt 
```