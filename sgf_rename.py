#!/usr/local/bin/python
import argparse
import sgf
import os

"""
Arguments
"""
parser = argparse.ArgumentParser()
parser.add_argument('--format', help='Format to rename SGFs, e.g. "{PB} - {PW}, {DT} - {RE}"', default="{PB} - {PW}, {DT} - {RE}")
parser.add_argument('SGF', metavar='SGF', nargs='+',
					help='SGF files you want to rename')

args = parser.parse_args()
sgfs = args.SGF

sgf_elements = ["AB", "AW", "AN", "AP", "B", "BR", "BT", "C", "CP", "DT", "EV", "FF", "GM", "GN", "HA", "KM", "ON", "OT", "PB", "PC", "PL", "PW", "RE", "RO", "RU", "SO", "SZ", "TM", "US", "W", "WR", "WT" ]
empty_sgf_dict = { k:"_" for k in sgf_elements}

for sgf_file in sgfs:
	with open(sgf_file) as sgf_reader:
		collection = sgf.parse(sgf_reader.read())
		properties_list = collection.children[0].nodes[0].properties
		properties_dict = {k:v[0] for k,v in properties_list.items()}
		properties_dict = {**empty_sgf_dict, **properties_dict}
		proposed_filename = args.format.format(**properties_dict)
		proposed_full_filename = "{}.sgf".format(proposed_filename)
		rename_attempt = 1
		if not os.path.exists(proposed_full_filename):
			os.rename(sgf_file, proposed_full_filename)
		else:
			while os.path.exists(proposed_full_filename):
				proposed_full_filename = "{}_{}.sgf".format(proposed_filename,rename_attempt)
				rename_attempt += 1
			os.rename(sgf_file, proposed_full_filename)